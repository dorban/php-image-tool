<?php
/*
 * COPYRIGHT (C) 2011 by HDLABS.DE
 * AUTHOR : STEPHAN PLOEHN
 * E-MAIL : STEPHAN@HDLABS.DE
 *
 * LATEST CHANGE : See SVN log
 */

// include the global file
include "includes/global.php";

// get image
$image_id = $_GET["name"];

$query  = sprintf("SELECT `name`, `image`, `description`, `date` FROM `hd_images` WHERE `name` = '%d' LIMIT 1", $image_id);
$result = $database->query($query);

// check if image was find
if($result->num_rows < 1)
    header("Location: ../");

$image = $result->fetch_array();

// check if image is reported
if(isset($_POST["report"]))
{
    $to       = $settings_email;
    $subject  = "Es wurde ein Bild gemeldet.";
    $message .= "Es wurde ein Bild gemeldet auf ".$settings_sitename."\n";
    $message .= "Bild: ".$settings_path."/img/".$image["name"]."\n";
    $message .= "Meldung: ".$_POST["description"]."\n\n";
    $message .= "Bitte kontrollieren!";

    // send mail
    mail($to, $subject, $message);
    // send xmpp message
    if($xmpp_message_report == 1)
    {
        $xmpp->message($xmpp_to, "Es wurde ein Bild gemeldet! Meldung: ".$_POST["description"].". Bitte kontrollieren unter: ".$settings_path."/img/".$image["name"]);
    }

    // template message
    $template->assign("report", true);
}

// display header
$template->assign("image", $image);
$template->assign("title", "Bild anzeigen");
$template->display("header.tpl"); 

// display content
$template->display("image.tpl");

// display footer
$template->display("footer.tpl");
ob_end_flush();
?>