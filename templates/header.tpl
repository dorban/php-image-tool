<!DOCTYPE HTML>
<html>
<head>
    <base href="{$path}/" />
    <title>{$title} - {$sitename}</title>
    <link rel="stylesheet" href="style/layout.css" type="text/css" />
    <link rel="stylesheet" href="style/style.css" type="text/css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#report_link').click(function() {
            $('#report').toggle('slow');
        });
        $('#delete_link').click(function() {
            $('#delete').toggle('slow');
        });
    });
    </script>
</head>

<body>
	<div class="Main">
		<div class="header-main">
		<h1 id="header">
		<a href="http://www.it-native.de/"><img src="http://www.it-native.de/logo.png"></a>
		<div class="description"></div>
		</h1>
	</div>
		<div class="Main2">