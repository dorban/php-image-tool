<h1>Bild "{$image.name}"</h1>

{if $report == true}
<div class="success">
    Der Seiteninhaber wurde benachrichtigt.
</div>
{/if}

<a href="images/{$image.image}">
    <img src="images/{$image.image}" alt="{$image.name}" id="preview_image" />
</a><br />
Bild-ID: {$image.name}<br />
Beschreibung: {$image.description}<br />
Hochgeladen am: {$image.date|date_format:"%d.%m.%Y - %H:%I Uhr"}<br />
<label for="image_here">Diese Seite:</label>
<input type="text" value="{$path}/img/{$image.name}" id="image_here" readonly /><br />
<label for="image_direct">Direktlink zum Bild:</label>
<input type="text" value="{$path}/images/{$image.image}" id="image_direct" readonly /><br />
<label for="image_bbcode">BBCode:</label>
<input type="text" value="[img]{$path}/images/{$image.image}[/img]" id="image_bbcode" readonly /><br />
<label for="image_html">Html Code:</label>
TODO!
<br /><br />
<div id="report_link">Bild melden</div>
<div id="report">
    <form action="img/{$image.name}" method="post">
        <label for="report_desc">Meldung:</label><br />
        <textarea name="description" id="report_desc">Dieses Bild enthält Inhalt der nicht sehr geeignet ist!</textarea><br />
        <input type="submit" value="Melden" name="report" />
    </form>
</div>
<div id="delete_link">Bild entfernen</div>
<div id="delete">
    <form action="./" method="post">
        Bild wirklich löschen?<br />
        <label for="user_admin">Passwort:</label>
        <input type="password" name="password" id="user_admin" /><br />
        <input type="hidden" name="image" value="{$image.name}" />
        <input type="hidden" name="image_file" value="{$image.image}" />
        <input type="submit" value="Löschen" name="delete" />
    </form>
</div>