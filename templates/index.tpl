<h1 align="center">{$sitename}</h1><br /><br />

{if $error_nopic == true}
<div class="error">
    Hast du nicht etwas vergessen?
</div>
{elseif $error_size == true}
<div class="error">
    Dein Bild ist zu groß!
</div>
{elseif $error_format == true}
<div class="error">
    Deine Datei ist kein Bild!
</div>
{elseif isset($deleted)}
<div class="success">
    Bild "{$deleted}" wurde erfolgreich gelöscht
</div>
{elseif $wrong_pass == true}
<div class="error">
    Das Passwort ist falsch! <a href="javascript:history.back()">Zurück</a>
</div>
{/if}
<div class="upload">
<form method="post" action="" enctype="multipart/form-data">

    <label for="image_file">Bild von der Festplatte auswählen:</label>
    <input type="file" name="image" id="image_file" /><br /><br />

    <label for="image_description">Beschreibung des Bildes:</label>
    <textarea id="image_description" name="desc">Keine Beschreibung</textarea><br />

	<p align="center">
    <input type="submit" value="Bild hochladen" name="upload"/>
	</p>
</form>


</div>
<h1>How to use:</h1>
<ul>
	<li>Jedes Bild kann maximal {$size} MByte groß sein.</li>
	<li>Die größe eines Bildes (Länge, Breite) ist egal.</li>
	<li>Folgende Formate sind erlaubt: JPG, JPEG, PNG, GIF, BMP</li>
	<li>Folgende Formate funktionieren, haben aber keine Vorschau: PSD, TIF</li>
	<li>Es ist nicht gestattet rechtsradikales Material, Kinderpronographie, oder sonstwie illegales Material hochzuladen und zu verteilen.</li>
</ul>

<br />

