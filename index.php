<?php
/*
 * COPYRIGHT (C) 2011 by HDLABS.DE
 * AUTHOR : STEPHAN PLOEHN
 * E-MAIL : STEPHAN@HDLABS.DE
 *
 * LATEST CHANGE : See SVN log
 */

// include the global file
include "includes/global.php";

// display header
$template->assign("title", "Bild hochladen");
$template->display("header.tpl");

if(isset($_POST['upload']))
{
    // check if a image input is no empty
    if(empty($_FILES['image']['name']))
    {
        $template->assign("error_nopic", true); // no pic selected
    }
    else
    {
        $type = getimagesize($_FILES['image']['tmp_name']);
        if ($type[2] != 0) {
            // check if imagesize is ok
            if ($_FILES['image']['size'] <= $settings_size)
            {
                // generate name
                $imgname = $_FILES['image']['name'];
                $filetype = pathinfo($imgname);
                $filetype = $filetype['extension'];
                $filename = date('ymd')."".mt_rand(1000,9999);
                $ip       = $_SERVER["REMOTE_ADDR"];

                // move image
                move_uploaded_file($_FILES['image']['tmp_name'], "images/".$filename.".".$filetype);

                // inser into db
                $query = sprintf("INSERT INTO `hd_images` (
                                    `name` ,
                                    `image` ,
                                    `description` ,
                                    `date` ,
                                    `remote_ip`
                                  )
                                  VALUES (
                                    '%d', '%s', '%s', CURRENT_TIMESTAMP , '%s'
                                  )", $filename, $filename.".".$filetype, $_POST["desc"], $ip);
                $database->query($query);

                // send xmpp message
                if($xmpp_message_new == 1)
                {
                    $xmpp->message($xmpp_to, "Es wurde ein Bild hochgeladen! ".$settings_path."/img/".$filename);
                }

                // go to image preview
                header("Location: img/".$filename);
            }
            else
            {
                $template->assign("error_size", true); // image is to big
            }
        }
        else
        {
            $template->assign("error_format", true); // wrong format
        }
    }
}

if(isset($_POST["delete"]))
{
    // check password
    if($_POST["password"] != $settings_password)
        $template->assign("wrong_pass", true);
    else
    {
        // delete from db
        $query = sprintf("DELETE FROM `hd_images` WHERE `name` = '%d' LIMIT 1", $_POST["image"]);
        $database->query($query);
        // delete from webserver
        unlink("images/".$_POST["image_file"]);

        // send xmpp message
        if($xmpp_message_deleted == 1)
        {
            $xmpp->message($xmpp_to, "Es wurde ein Bild hochgeladen! ".$_POST["image"]);
        }

        // send message to template
        $template->assign("deleted", $_POST["image"]);
    }
}

// display content
$template->display("index.tpl");

// display footer
$template->display("footer.tpl");
ob_end_flush();
?>