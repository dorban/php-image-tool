<?php
/*
 * COPYRIGHT (C) 2011 by HDLABS.DE
 * AUTHOR : STEPHAN PLOEHN
 * E-MAIL : STEPHAN@HDLABS.DE
 *
 * LATEST CHANGE : See SVN log
 */

$connect_file = true;

// include the global file
include "includes/connect.php";
include "includes/settings.php";
include "libs/smarty/Smarty.class.php";
include "libs/xmpphp/XMPP.php";

// remove html/php tags from inputs
foreach($_POST as $key => $value)
    $_POST[$key] = strip_tags($value);

// Save DB Query
foreach($_POST as $key => $value)
    $_POST[$key] = $database->real_escape_string($value);
foreach($_GET as $key => $value)
    $_GET[$key] = $database->real_escape_string($value);
foreach($_COOKIE as $key => $value)
{
    if(get_magic_quotes_gpc()) $_COOKIE[$key]=stripslashes($value);
        $_COOKIE[$key] = $database->real_escape_string($value);
}

// render page if its done
ob_start();

// show max size in mb
$mb_size = $settings_size / 1024 / 1024;

// get new Smarty
$template = new Smarty;
$template->assign("sitename", $settings_sitename);
$template->assign("email", $settings_email);
$template->assign("owner", $settings_owner);
$template->assign("size", $mb_size);
$template->assign("path", $settings_path);

// create new xmpp
if($xmpp_message_report  == 1 or
   $xmpp_message_new     == 1 or
   $xmpp_message_deleted == 1)
{
    $xmpp = new XMPPHP_XMPP($xmpp_from_server, $xmpp_from_port, $xmpp_from_username, $xmpp_from_password, "xmpphp", $xmpp_from_server);
    $xmpp->connect();
    $xmpp->processUntil('session_start');
}
?>