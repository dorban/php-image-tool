<?php
/*
 * COPYRIGHT (C) 2011 by HDLABS.DE
 * AUTHOR : STEPHAN PLOEHN
 * E-MAIL : STEPHAN@HDLABS.DE
 *
 * LATEST CHANGE : See SVN log
 */

if($connect_file != true)
    die();

// --------------------------------------------------------------------------- [ main settings ]
$settings_sitename       = "Imagehosting by IT-Native.de";                      // name of the page
$settings_password       = "";                        // password to delete a image
$settings_size           = 8388608;                             // image size in bytes (http://www.big-foot.de/tests/bitsrechner.htm)
$settings_email          = "e";                // site email
$settings_owner          = "";                     // site owner
$settings_path           = "";   // path to this script. WHITOUT last trailing slash

// ---------------------------------------------------------------------------- [ xmpp support ]
// node : you must set up 2 jabber accounts. one from wich the message should send (from)
//        and one to receive it (to).
$xmpp_message_report     = 1;                                   // send message if image was reportet
$xmpp_message_new        = 1;                                   // send message if image was uploaded
$xmpp_message_deleted    = 1;                                   // send message if image was deleted
$xmpp_from_username      = "";                             // from username
$xmpp_from_server        = "";             // from server
$xmpp_from_password      = "";                       // from password
$xmpp_from_port          = 5222;                                // from port (no SSL support!)
$xmpp_to                 = "";             // to jabber-id

error_reporting(E_ALL ^ E_NOTICE);                              // Fehlermeldungen (Debug) (http://php.net/manual/de/function.error-reporting.php)
?>
