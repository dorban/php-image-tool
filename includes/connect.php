<?php
/*
 * COPYRIGHT (C) 2011 by HDLABS.DE
 * AUTHOR : STEPHAN PLOEHN
 * E-MAIL : STEPHAN@HDLABS.DE
 *
 * LATEST CHANGE : See SVN log
 */

if($connect_file != true)
    die();

// connect settings
$db_server  = "";
$db_user    = "";
$db_pass    = "";
$db_name    = "";

// create new mysqli database connection
$database   = new mysqli($db_server, $db_user, $db_pass, $db_name);

// check mysqli connection
if(mysqli_connect_errno() != 0)
{
    echo "Finde kein Weg zur Datebank :( <br /><strong>Fehlercode: ".mysqli_connect_errno()."</strong>";
    die();
}

// Encoding
$database->set_charset("utf8");

header('content-type: text/html; charset=utf-8');
setlocale (LC_ALL, 'de_DE@euro', 'de_DE', 'de', 'ge');
?>
